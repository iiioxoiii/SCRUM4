import java.util.Arrays;

public class scrum4 {

    /**
     * Assignació de cada funció a cadascú de l’equip:
     *
     * 1)El professor ha impartit més vegades el M3 de DAM - Amós
     *
     * 2)Quants antics alumnes en total existeixen per cada cicle de l’institut
     * introduint en aquest cas les tuples (nom alumne, cicle formatiu). - Oriol
     *
     * 3)El professor que ha estat més anys a l’institut. - Johnnatan&Kevin
     *
     **/


    static String[][] llistaProfes = {

            {"Juan Izquierdo", "M", "M3", "DAM", "2001"},
            {"Xavi Mohamed", "M", "M3", "DAM", "2010"},
            {"Lluerna Salada", "F","M5", "DAW", "1999"},
            {"Juan Izquierdo", "M","M2", "DAM", "2007"},
            {"Juan Izquierdo", "M","M3", "DAM", "2005"}
    };


    static String[][] llistaAlumnes = {
            {"Joan Fernandez", "M","DAM"},
            {"David Oller", "M" ,"DAW"},
            {"Kevin Sanz", "F","DAW"},
            {"Sonia Callejón", "F","LOL"},
            {"JLuisa Paredes", "F","DAW"},
    };


    static String[] llistaCicles = {"DAM","DAW","LOL","IPS"};





    public static void main(String args[]) {

        //El profe que ha impartit més vegades M3 de DAM (Amós)
        profeMesVegades(llistaProfes, "M3", "DAM");

        //Els alumnes antics que han passat pel l'escola (Oriol)
        anticsAlumnes(llistaAlumnes);

        //Els alumnes antics que han passat per l'escola-v2 (Amós)
        anticsAlumnes_v2(llistaAlumnes,llistaCicles);

        //Pinta el el profe que
        buscadorEdad(llistaProfes);

        //Pinta llista profes per sexe
        profesPerSexe(llistaProfes, "M");

        //Pinta llista alumnes per sexe
        alumnesPerSexe(llistaAlumnes, "F");

    }



    public static void profesPerSexe(String [][] entrada , String sexe){

        String s;

        for (int i = 0; i < entrada.length; i++) {
            s = entrada[i][1];
            if (s.equals(sexe)) {
                System.out.println("LLista de profes "+ sexe);
                System.out.println(llistaProfes[i][1]);
            }
        }


    }


    /**
     * @param sexe
     */
    public static void alumnesPerSexe(String [][] entrada, String sexe){

        String s;

        for (int i = 0; i < entrada.length; i++) {
            s = entrada[i][1];
            if (s.equals(sexe)) {
                System.out.println("LLista d'alumnes "+ sexe);
                System.out.println(llistaAlumnes[i][1]);
            }
        }


    }







    //Quants antics alumnes en total existeixen per cada cicle de l’institut
    // introduint en aquest cas les tuples (nom alumne, cicle formatiu). - Oriol

    /**
     * @param array
     */
    public static void anticsAlumnes(String[][] array) {
        int contDAM = 0;
        int contDAW = 0;

        String[] resultat = new String[10];

        for (int i = 0; i < llistaAlumnes.length; i++) {
            resultat[i] = llistaAlumnes[i][1];
            if (resultat[i] == "DAM") {
                contDAM++;
            } else if (resultat[i] == "DAW") {
                contDAW++;
            }
        }
        System.out.println("Alumnes per cicle:");
        System.out.println("El cicle DAM te " + contDAM + " alumnes");
        System.out.println("El cicle DAW te " + contDAW + " alumnes");
    }




    //Conpta els alumnes de cada cicle. Els alumnes es passen en el primer paràmetre i el llistat de cicles
    /**
     * @param array_a {nom, cicle}
     * @param array_b {cicle}
     */
    public static void anticsAlumnes_v2(String[][] array_a, String [] array_b) {

        int [] arrayAuxResultats=new int[array_b.length];

        for (int i = 0; i < array_b.length; i++) {
            int c = 0;
            for (int j = 0; j < array_a.length; j++) {
                if(array_a[j][1].equals(array_b[i])){
                    //System.out.println("array_a["+ j+"][1]="+array_a[j][1]+ " arrayAuxResultats[i]"+ arrayAuxResultats[i]);
                    c++;
                }
            }
            arrayAuxResultats[i]=c;
        }
        System.out.println();
        System.out.println("Alumnes per cicle(v2):");
        for (int i = 0; i < arrayAuxResultats.length ; i++) {
            System.out.println("El cicle " + array_b[i] + " l'han cursat " + arrayAuxResultats[i]+ " alumnes.");
        }
        System.out.println();

    }


    /**
     * @param a Llista de profes
     */
    //El professor que ha estat més anys a l’institut. -Johnnatan
    public static void buscadorEdad(String[][] a) {

        String[] b = new String[a.length];
        String nomprofe="";
        String  anyprofe="";
        for (int c = 0; c < a.length; c++) {
            b[c] = (a[c][3]);
        }

        Arrays.sort(b);

        for (int c = 0; c < a.length; c++) {

            if (b[0] == a[c][3]) {
                for (int d = 0; d < a[c].length; d++) {
                    nomprofe=a[c][0];
                    anyprofe=a[c][3];

                }
            }
        }System.out.println("El profe que porta mes anys es:");
        System.out.println(nomprofe + " desde l'any "+ anyprofe);
        System.out.println();
    }


    /**
     * @param array llistat de profes
     * @param modul llistat de moduls
     * @param cicle llistat de cicles
     */
    public static void profeMesVegades(String array[][], String modul, String cicle ) {

        //Es crea un array per posar la tupla coincident amb condició "mòdul=M3 i cicle=DAM"
        int num = 0;

        for (int i = 0; array.length > i; i++) {

            //S'avalua la condició
            if (array[i][1].equals(modul) && array[i][2].equals(cicle)) {
                num++;
            }
        }

        //Es crea un array per posar els noms dels profes coincidents amb la condició
        // de la mida que hem calculat amb la var num
        String profesM3DAM[] = new String[num];


        //Es copien els noms al nou array fent una altra vegada la rutina...
        int z = 0;

        for (int i = 0; i < array.length; i++) {

            //S'avalua la condició i es copia el nom al nou array
            if (array[i][1].equals(modul) && array[i][2].equals(cicle)) {
                profesM3DAM[z] = array[i][0];
                z++;
            }
        }

        //Vale! Ya tenim un array de puta mare amb tots els profes coincidents
        //System.out.println("Les coincidències de profes a M3 i DAM són: ");

        //for (String a : profesM3DAM) {
        //    System.out.println(a);
        //}


        //Ara creem un array on apuntarem amb un enter les coincidències de tots entre tots
        //El que tingui més coincidències és el que ha fet més cursos.
        int[] repeticioNoms = new int[profesM3DAM.length];

        //Primer l'omplim amb 0;
        for (int i : repeticioNoms) {
            i = 0;

        }

        // /Després comparem tots els elements amb tots apuntem el valor de coincidència de
        // cada element

        for (int i = 0; i < profesM3DAM.length; i++) {

            String nom = profesM3DAM[i];
            for (int x = 0; x < profesM3DAM.length; x++) {

                String nom2 = profesM3DAM[x];

                if (nom2.equals(nom)) {
                    repeticioNoms[i] = repeticioNoms[i] + 1;
                }
            }
        }


        //Cerquem el número més alt de
        System.out.println("El profe que més vegades ha donat M3 de DAM es:");

        int max = 0;

        for (int i : repeticioNoms) {
            if (i > max) {

                max = i;
            }
        }//Finalment pintem el nom del "registre" amb valor "max".
        boolean sortir = false;
        for (int i = 0; (i < repeticioNoms.length) && !sortir; i++) {
            if (repeticioNoms[i] == max) {

                System.out.println(profesM3DAM[i]);
                System.out.println();

                sortir = true;
            }
        }

    }
}
